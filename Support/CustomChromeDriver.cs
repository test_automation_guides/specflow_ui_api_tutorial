using Coypu.Drivers;
using Coypu.Drivers.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace specflow_tut.Support
{
	public class CustomChromeDriver : SeleniumWebDriver
	{
		private static ChromeDriver chromedriver;

		public CustomChromeDriver()	: base(CustomProfileDriver(), Browser.Chrome)
		{
		}

		public new void Dispose()
		{
			chromedriver.Dispose();
			base.Dispose();
		}

		private static RemoteWebDriver CustomProfileDriver()
		{
			ChromeOptions options = new ChromeOptions();
			options.AddArgument("--no-sandbox");
			options.AddArgument("--disable-dev-shm-usage");
			chromedriver = new ChromeDriver(".", options);            
			return chromedriver;
		}
	}
}