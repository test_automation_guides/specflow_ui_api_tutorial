using Coypu;
using Coypu.Drivers;
using specflow_tut.Support;
using TechTalk.SpecFlow;

namespace specflow_tut.StepDefinitions
{
	[Binding]
	public class PlayerSteps
	{
		[Given(@"I am player X")]
		public void GivenIAmPlayerX()
		{

		}

		[When(@"I am playing tick tack toe")]
		public void WhenIAmPlayingTickTackToe()
		{
			var sessionConfiguration = new SessionConfiguration()
			{
				Browser = Browser.Chrome
			};
			var chromedriver = new CustomChromeDriver();
          	var browser = new BrowserSession(sessionConfiguration, chromedriver);
			browser.Visit("https://test_automation_guides.gitlab.io/tick_tack_toe_react/");
			browser.Dispose();
		}
	}
}