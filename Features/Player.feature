Feature: Player
	As a player I want to know when it is my turn. The game starts with player X
	and then goes on to player O

Scenario: X starts the game
	Given I am player X
	When I am playing tick tack toe
	Then the current turn is player 'O'

Scenario: O turn after X
	Given I am player X
	And I am playing tick tack toe
	When I make a move
	Then the current turn is player 'O'
